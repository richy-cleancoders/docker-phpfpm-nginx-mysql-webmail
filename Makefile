#--------------------------------------------------- VARIABLES --------------------------------------------------------#
#---DOCKER---#
DOCKER = docker
DOCKER_COMPOSE = docker compose
DOCKER_NETWORK = app_dev
ENVIRONMENTS_VARIABLES_FILES = .env
#------------#

##
## HELP
help: ## Show this help.
	@echo "Command helper"
	@echo "---------------------------"
	@echo "Usage: make [target]"
	@echo ""
	@echo "Targets:"
	@grep -E '(^[a-zA-Z0-9_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}{printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'

.PHONY: info

##
## Docker command
build: ## Build application
	@$(DOCKER) network ls|grep $(DOCKER_NETWORK) > /dev/null || (echo "Create docker network <$(DOCKER_NETWORK)>" && $(DOCKER) network create --driver bridge $(DOCKER_NETWORK))
	@echo "Build application"
	@cd ./docker && $(DOCKER_COMPOSE) pull --parallel --quiet --ignore-pull-failures 2> /dev/null
	@cd ./docker && $(DOCKER_COMPOSE) build --pull

kill: ## Kill application container
	@echo "Kill application"
	@cd ./docker && $(DOCKER_COMPOSE) kill
	@cd ./docker && $(DOCKER_COMPOSE) down --volumes --remove-orphans
	@$(DOCKER) network rm $(DOCKER_NETWORK)
	@cd ./docker && $(DOCKER_COMPOSE) down --volumes

start: ## Start app application
	@cd ./docker && $(DOCKER_COMPOSE) up -d --remove-orphans --no-recreate

stop: ## Stop app application
	@cd ./docker && $(DOCKER_COMPOSE) stop

connect-to-app: ## Connect to app app
	@$(DOCKER) exec -it application bash

list-containers: ## List app containers
	@cd ./docker && $(DOCKER_COMPOSE) ps -a

.PHONY: build kill start stop connect-to-app list-containers

##
## Logs
app-logs: ## Shows logs from the app container. Use ctrl+c in order to exit
	@$(DOCKER) logs -f application

database-logs: ## Shows logs from the mysql container. Use ctrl+c in order to exit
	@$(DOCKER) logs -f database

nginx-logs: ## Shows logs from the nginx container. Use ctrl+c in order to exit
	@$(DOCKER) logs -f nginx

mailer-logs: ## Shows logs from the mailer container. Use ctrl+c in order to exit
	@$(DOCKER) logs -f mailer

.PHONY: app-logs database-logs nginx-logs mailer-logs
#---------------------------------------------#