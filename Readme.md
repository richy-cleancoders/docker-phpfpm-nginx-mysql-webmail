# Docker Php-fpm nginx mysql webmail configuration

This repository contains some services to start a new php project.

## Requirements

You should have:

- Docker
- Docker Composer
- Make utilities

## Installation

| **Tools**      | **Version**                                                                |
|----------------|----------------------------------------------------------------------------|
| **PHP-FPM**    | 8.2.12                                                                     |
| **NGINX**      | 1.25.3                                                                     |
| **COMPOSER**   | 2.6.5                                                                      |
| **XDEBUG**     | 3.2.2                                                                      |
| **MYSQL**      | 8.2.0 - MySQL Community Server - GPL  Protocol Version: 10                 |
| **WEBMAIL**    | Roundcube Webmail 1.3.1                                                    |
| **PHPMYADMIN** | Apache/2.4.57 (Debian) Database client version : libmysql - mysqlnd 8.2.13 |

Firstly, you have to clone the repository:

```
> git clone https://gitlab.com/richy-cleancoders/docker-phpfpm-nginx-mysql-webmail.git
> cd docker-phpfpm-nginx-mysql-webmail
```

All docker configurations are available in the `docker` folder.

- `nginx`: in `docker/nginx` folder contains nginx configuration

If you want to modify your application root directory, you have to change `root` option into `docker/nginx/templates/default.conf.template` file

```
server {
    listen 80;
    root /var/www/html/public;

    location / {
        try_files $uri /index.php$is_args$args;
    }

    location ~ ^/index\.php(/|$) {
        if ($request_method = 'OPTIONS') {
            add_header 'Access-Control-Allow-Origin' '*' always;
            add_header 'Access-Control-Allow-Methods' 'GET, POST, DELETE, OPTIONS' always;
            add_header 'Access-Control-Allow-Headers' 'Authorization,DNT,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Range' always;
            add_header 'Access-Control-Max-Age' 1728000 always;
            add_header 'Content-Type' 'text/plain; charset=utf-8' always;
            add_header 'Content-Length' 0 always;
            return 204;
        }
        add_header 'Access-Control-Allow-Origin' '*' always;
        add_header 'Access-Control-Allow-Methods' 'GET, POST, DELETE, OPTIONS' always;
        add_header 'Access-Control-Allow-Headers' 'Authorization,DNT,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Range' always;
        add_header 'Access-Control-Expose-Headers' 'Content-Length,Content-Range' always;

        fastcgi_pass application:9000;
        fastcgi_split_path_info ^(.+\.php)(/.*)$;
        include fastcgi_params;
        fastcgi_param SCRIPT_FILENAME $realpath_root$fastcgi_script_name;
        fastcgi_param DOCUMENT_ROOT $realpath_root;
        internal;
    }

    location ~ \.php$ {
        return 404;
    }

    error_log /dev/stdout info;
    access_log /var/log/nginx/access.log;
}
```

- `php-fpm`: in `docker/php-fpm` folder contains php-fpm configuration

You also can modify the `Dockerfile` configuration to add or install some required packages that you need for your project.

All services are available in the `docker/docker-compose.yaml` file and environment variables in `docker/.env` file

## Build

Now, you can build the project with docker.

```
> make build
> make start
> make connect-to-app
```

```
> database: represent mysql database
> phpmyadmin: to look database content
> application: represent your application
> nginx: represent the server configuration
> mailer: represent mailer service to send and receive email locally
```

## Available urls

All available urls with credentials for each service.

```
> application

url: http://localhost:4006
```

```
> phpmyadmin

url: http://localhost:3310

Server: database
username: application
password: password
```

```
> mailer

webmail url: http://localhost:8130
smtp server: smtp://mailer:2535

username: user
password: password
```